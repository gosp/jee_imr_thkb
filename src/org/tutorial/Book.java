package org.tutorial;

public class Book {
	public static int COUNT = 0;
	private int id;
	
	private String title;
	private String author;
	public Book(String t, String a){
		COUNT++;
		id=COUNT;
		title=t;
		author=a;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
}
