package org.tutorial;

import java.util.ArrayList;
import java.util.List;

public class BookDAOMockImpl implements BookDAO {
	public List<Book> FindByAll() {
		List<Book> l = new ArrayList<Book>();
		l.add(new Book("Cuisine n�1", "Jean Bon"));
		l.add(new Book("Cuisine n�2", "So. Sisse"));
		return l;
	}
}
