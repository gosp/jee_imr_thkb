package org.tutorial;
import java.util.List;

public interface UserDAO {
	public abstract List<User> FindByAll();
	
	public abstract boolean checkConnection(String login, String password);
}
