package org.tutorial;
import java.util.List;

public class UserServiceImpl implements UserService {

	private UserDAO userDao = new UserDAOMockImpl();
	
	public List<User> getAllUsers() {
		return userDao.FindByAll();
	}
	
	public boolean checkUser(String login, String password) {
		return userDao.checkConnection(login, password);
	}

}
