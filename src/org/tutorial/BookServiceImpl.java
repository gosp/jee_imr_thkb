package org.tutorial;

import java.util.List;

public class BookServiceImpl implements BookService{

	private BookDAO bookDao = new BookDAOImpl();
	
	public List<Book> getAllBooks() {
		return bookDao.FindByAll();
	}

}
