package org.tutorial;

import java.util.ArrayList;
import java.util.List;

public class UserDAOMockImpl implements UserDAO {
	
	public List<User> FindByAll() {
		List<User> l = new ArrayList<User>();
		l.add(new User("kevin", "patate"));
		l.add(new User("tony", "frite"));
		return l;
	}
	
	public boolean checkConnection(String login, String password) {
		
		List<User> l = FindByAll();
		boolean result = false;
		
		for (User user:l) {
			if(user.getPseudo().equals(login) && user.getPassword().equals(password))
			{
				result = true;
			}
		}
		
		return result;
		
	}

}
