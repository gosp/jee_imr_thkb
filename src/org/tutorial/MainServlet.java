package org.tutorial;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServlet;

/**
 * Servlet implementation class MainServlet
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
        //lol
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doProcess(request, response);
	}
	
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		
		// initialisation de la session
		HttpSession session = request.getSession();
		String pageName = "/connexion.jsp";

		RequestDispatcher rd = null;
		
		if(request.getParameter("page") != null)
		{
			if(request.getParameter("page").equals("connexion"))
			{
				if(session.getAttribute("connected") == null || !session.getAttribute("connected").equals("1"))
				{				
					pageName="/connexion.jsp";
					UserServiceImpl userServiceImpl = new UserServiceImpl();				
					boolean user_exists = userServiceImpl.checkUser(request.getParameter("login"), request.getParameter("password"));
					if(user_exists)
					{
						System.out.println("Vous �tes connect�");
						
						// Set des variables user en session
						session.setAttribute("connected", "1");
						session.setAttribute("login", request.getParameter("login"));
						
						pageName = "/accueil.jsp";
						BookServiceImpl bookServiceImpl = new BookServiceImpl();
						List<Book> listBooks = bookServiceImpl.getAllBooks();
						request.setAttribute("listBooks", listBooks);
					}
					else
					{
						System.out.println("Vous n'�tes pas connect�");
						pageName = "/connexion.jsp";
					}
				}
				else
				{
					pageName="/accueil.jsp";
					BookServiceImpl bookServiceImpl = new BookServiceImpl();
					List<Book> listBooks = bookServiceImpl.getAllBooks();
					request.setAttribute("listBooks", listBooks);
				}
			}
			else if(request.getParameter("page").equals("accueil"))
			{
				pageName="/accueil.jsp";
				BookServiceImpl bookServiceImpl = new BookServiceImpl();
				List<Book> listBooks = bookServiceImpl.getAllBooks();
				request.setAttribute("listBooks", listBooks);
			}		
			else if(request.getParameter("page").equals("profil"))
			{
				pageName="/profil.jsp";
				request.setAttribute("username", session.getAttribute("login"));
			}	
			else
			{
				System.out.println("erreur");
			}
		}
		else
		{
			System.out.println(session.getAttribute("connected"));
			pageName="/connexion.jsp";
		}
		
		rd = getServletContext().getRequestDispatcher(pageName);
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
