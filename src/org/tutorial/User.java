package org.tutorial;

public class User {
	public String pseudo;
	private String password;

	public User(String pseudo, String pass){
		this.pseudo = pseudo;
		this.password = pass;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}

}
