package org.tutorial;
import java.util.List;

public interface UserService {
	List<User> getAllUsers();
	
	boolean checkUser(String login, String password);
}
