package org.tutorial;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.Statement;


public class BookDAOImpl implements BookDAO{
	
	private List<Book> listBooks;
	private Connection connexion;
	
	public BookDAOImpl(){
		listBooks = new ArrayList<Book>();
		connexion = DBManager.getInstance().getConnection();
		
	}
	public List<Book> FindByAll() {	
		try {
			Statement statement = connexion.createStatement();
			ResultSet rs = statement.executeQuery("select * from book");
			while (rs.next()){
				String title = rs.getString("titre");
				String auteur = rs.getString("auteur");
				listBooks.add(new Book(title, auteur));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listBooks;
	}
}
